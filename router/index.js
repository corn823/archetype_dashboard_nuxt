export default ({
  defaultRoutes: [
    {
      path: '/pc/Login',
      name: 'pc-Login',
      title: 'PC登入'
    }
  ],
  defaultRoutesMobile: [
    {
      path: '/mobile/login',
      name: 'mobile-Login',
      title: 'Mobile登入'
    }
  ],
  mainRoutes: [
    {
      path: '/pc/Main',
      name: 'pc',
      children: [
        // 首頁
        { path: '/index', title: 'PC首頁', name: 'pc-Index' },
        // 測試頁
        { path: '/test', title: 'PC測試頁', name: 'pc-Test' }
      ]
    }
  ],
  mainRoutesMobile: [
    {
      path: '/mobile/Main',
      name: 'mobile',
      children: [
        // 首頁
        { path: '/mobile/Index',  title: 'Mobile首頁', name: 'index' },
        // 測試頁
        { path: '/mobile/Test',  title: 'Mobile測試頁', name: 'test' }
      ]
    }
  ],
  routesMapping (level, isPhone) {
    let auths = []
    const routes = isPhone ? this.mainRoutesMobile : this.mainRoutes
    switch (level) {
      case 'RT':
        auths = routes[0].children.map(item => { return item.id })
        break
    }
    return auths
  },
  getRoutes (auths, isPhone) {
    var json = auths
    const routes = isPhone ? this.mainRoutesMobile : this.mainRoutes
    const newChildren = routes[0].children.filter(route => {
      for (var ind in json) {
        if (json[ind] === route.id) {
          return true
        }
      }
      return false
    })
    return newChildren
  }
})
