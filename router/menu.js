export function getMenu (newRoutes) {
  const emptyMenu = [
    {
      name: 'index',
      item: []
    },
    {
      name: 'test',
      item: []
    }
  ]
  const menu = emptyMenuPush(emptyMenu, newRoutes)
  return menu
}
function emptyMenuPush (emptyMenu, newRoutes) {
  newRoutes.forEach(item => {
    delete item.component
    const index = item.id.split('-')[0] - 1
    emptyMenu[index].item.push(item)
  })
  emptyMenu = emptyMenuFilter(emptyMenu)
  return emptyMenu
}

function emptyMenuFilter (emptyMenu) {
  emptyMenu = emptyMenu.filter(item => item.item[0])
  return emptyMenu
}
