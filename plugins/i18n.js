import Vue from 'vue'
import VueI18n from 'vue-i18n'

import tw from 'view-design/dist/locale/zh-TW'
import en from 'view-design/dist/locale/en-US'

import langTW from '~/lang/tw'
import langEn from '@/lang/en'

import { localeMap } from '@/util/locale'

Vue.use(VueI18n)
Vue.locale = () => { }
const messages = {
  English: Object.assign(langEn, en),
  繁中: Object.assign(langTW, tw)
}

// const locale = sessionStorage.getItem('locale')
const locale = 'zh-tw'
const i18n = new VueI18n({
  locale: locale === null ? '中文' : localeMap[locale], // set locale
  messages // set locale messages
})

Vue.prototype._i18n = i18n

export default i18n
