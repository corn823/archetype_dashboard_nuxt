
// export default {
//   asyncData (context) {
//     console.log('req.header', context)
//   }
// }



// import routes from '@/router/index'
// import Router from 'vue-router'

// let isPhone = false
// const isPhoneLastTime = JSON.parse(sessionStorage.getItem('isPhoneLastTime'));

// (function () {
//   const device = navigator.userAgent
//   if (device.includes('Mobile')) {
//     isPhone = true
//   }
//   sessionStorage.setItem('isPhoneLastTime', isPhone)
//   compareWithIsPhone()
// }
// )()

// // 若手機（平板）切換，則自動登出
// async function compareWithIsPhone () {
//   if (isPhoneLastTime !== null) {
//     if (isPhoneLastTime !== isPhone) {
//       try {
//         await store.dispatch('Logout')
//         location.reload()
//       } catch (err) {
//         console.log(err)
//         location.reload()
//       }
//     }
//   }
// }

// const defaultRoute = isPhone ? routes.defaultRoutesMobile : routes.defaultRoutes

// const router = new Router({
//   base: '',
//   routes: defaultRoute
// })

// // 避免報Uncaught (in promise) NavigationDuplicated
// const originalPush = Router.prototype.push
// Router.prototype.push = function push (location) {
//   return originalPush.call(this, location).catch(err => err)
// }

// router.beforeEach((to, from, next) => {
//   const level = sessionStorage.getItem('level')
//   // const userpermissions = sessionStorage.getItem('userpermissions')
//   const auths = routes.routesMapping(level, isPhone)
//   sessionStorage.setItem('currentPath', to.path)
//   sessionStorage.setItem('currentName', to.name)
//   if (level) {
//     if (to.path === defaultRoute[0].path) {
//       next()
//     } else {
//       if (store.getters.newRouter.length === 0) {
//         const newRoutes = routes.getRoutes(auths, isPhone)
//         sessionStorage.setItem('newRoutes', JSON.stringify(newRoutes))
//         const newRouter = isPhone ? routes.mainRoutesMobile : routes.mainRoutes
//         newRouter[0].children = newRoutes
//         router.addRoutes(newRouter)
//         store.dispatch('addRoutes', newRouter).then(res => {
//           setTimeout(() => {
//             next({
//               ...to, replace: true
//             })
//           }, 500)
//         }).catch((err) => {
//           console.log(err)
//         })
//       } else {
//         if (to.matched.length < 2) {
//           next(from.path)
//         } else {
//           next()
//         }
//       }
//     }
//   } else {
//     if (to.path === defaultRoute[0].path) {
//       next()
//     } else {
//       next(defaultRoute[0].path)
//     }
//   }
// })

// /* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   store,
//   i18n,
//   components: { App },
//   // template: '<App/>'
//   render: h => h(App)
// })
