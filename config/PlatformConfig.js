import yuanbaoBackground from '../assets/platformConfig/yuanbao-background.png'
import yuanbaoLogo from '../assets/platformConfig/logo_yb.png'
import yuanbaoLogoMobile from '../assets/platformConfig/yuanbao-logo-mobile.png'
import yuanbaoBackgroundMobile from '../assets/platformConfig/yuanbao-background-mobile.png'

let platformConfig = {}
const beforeOnlineConfig = {
  title: 'beforeOnline'
}
const devConfig = {
  background: yuanbaoBackground,
  backgroundMobile: yuanbaoBackgroundMobile,
  logo: yuanbaoLogo,
  logoMobile: yuanbaoLogoMobile,
  appId: 'yuanbao-id',
  locale: ['简中', '繁中'],
  title: 'devTitle'
}
switch (process.env.VUE_APP_TITLE) {
  case 'beforeOnline':
    platformConfig = beforeOnlineConfig
    break
  default:
    platformConfig = devConfig
}
export default platformConfig
