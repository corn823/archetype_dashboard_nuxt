import axios from 'axios'
import { config } from './config'

axios.defaults.baseURL = config.baseURL
axios.interceptors.request.use(config => {
  config.headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-header': '*'
  }
  return config
}, err => {
  return Promise.reject(err)
})

export default axios
