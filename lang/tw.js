export default {
  account: '登入帳號',
  pw: '登入密碼',

  // login
  loginAccount: '帳戶',
  loginPW: '密碼',
  loginConfirm: '送出',
  loginFail: '登入失敗',

  // main
  logout: '登出',
  index: '首頁',
  test: '測試頁'
}
