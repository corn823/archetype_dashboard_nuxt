export const localeMap = {
  zh: '简中',
  简中: '简中',
  'zh-tw': '繁中',
  繁中: '繁中',
  en: 'English',
  English: 'English',
  th: 'ภาษาไทย',
  ภาษาไทย: 'ภาษาไทย',
  vi: 'Tiếng Việt',
  'Tiếng Việt': 'Tiếng Việt'
}
