
export default function (context) {
  let isMobile = (ua) => {
    return ua.match(/AppleWebKit.*Mobile.*/)
  }
  let userAgent = context.req ? context.req.headers['user-agent'] : navigator.userAgent
  if (isMobile(userAgent)) {
    // true的話，導入Mobile的Login
    return context.redirect('/mobile/Login')
  } else {
    // false的話，導入Pc的Login
    return context.redirect('/pc/Login')
  }
}
